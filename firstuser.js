require('./config/config');
require('./config/database');
require('./config/passport')

const mongoose = require('mongoose');
const User = mongoose.model('User');

let user = new User({
    username: "admin",
    password: "admin",
    role: 'admin'
})

user.save((err, nuser) => {
    if (err) {
        console.log(err)
        process.exit(0);
    } else {
        console.log("New user: ", nuser)
        process.exit(0);
    }
})