require('./config/config');
require('./config/database');
require('./config/passport');
const express = require('express');
const app = express();
const http = require('http').Server(app);
require('./appio')(http)

const EngineService = require('./src/engine/service')
const OrderService = require('./src/order/service')

setTimeout(() => {
    let engine = EngineService.engineSockets.reduce(OrderService.getTheEngine);
    console.log(engine);
    process.exit(0);
}, 3000);
