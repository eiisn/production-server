const passport = require('passport');
const mongoose = require('mongoose');
const LocalStrategy = require('passport-local').Strategy;

let User = mongoose.model('User');
const notFoundMessage = { message: "Combo utilisateur / mot de passe introuvable" }

passport.use(
    new LocalStrategy((username, password, done) => {
        User.findOne({username: username}, (err, user) => {
            if (err)
                return done(err);
            else if (!user) // unknown user
                return done(null, false, notFoundMessage);
            else if (!user.verifyPassword(password)) // wrong password
                return done(null, false, notFoundMessage);
            else // authentication succeeded
                return done(null, user);
        });
    })
);
