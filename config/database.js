const mongoose = require('mongoose');

mongoose.connect(process.env.MONGODB_URI, {useNewUrlParser: true, useUnifiedTopology: true}, (err) => {
    if (!err) {
        console.log('[MongoDB] connexion succeeded.')
    } else {
        console.log('[MongoDB] Error in connexion : ' + JSON.stringify(err, undefined, 2));
    }
});

mongoose.set('useCreateIndex', true);

require('../src/user/model');
require('../src/engine/model');
require('../src/order/model');
require('../src/product/model');