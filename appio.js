const mongoose = require('mongoose');
const Engine = mongoose.model('Engine');
const EngineService = require('./src/engine/service')


module.exports = (server) => {
    
    const io = require('socket.io')(server);
    const nspServer = io.of('/engine');

    EngineService.setServer(nspServer);

    Engine.find({}, (err, engines) => {
        if (err) {
            console.log(err)
        } else {
            engines.forEach((engine) => {
                EngineService.openConnexion(engine, nspServer);
            });
        }
    })
}
