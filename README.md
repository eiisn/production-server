# Départ :
> - Decoupeuse de savon
> - Presse mazzoni
> - Boudineuse simplex
> - Chignonneuse
> - Fondoir vapeur / diessel (1200 litres / 380 litres / 111 litres)
> - Machine à émulsion

## Après :
> 1. Conditionnement (machine qui conditionne les produits)
> 2. Encartonnage (machine qui met en boîte les produits)
> 3. Mise en quarentaine (Tapis qui emmène jusqu'a l'espace de quarentaine) (attente des résultats de la qualité)
>> Faire partir un echantillon en Qualité
> 4. Zone d'expedition (Tapis qui emmène dans la zone) (envoie des colis en entrepôt ou magasin ou particulier)

## Liens utiles : 
> - [Google: etapes production produit de beauté](https://www.google.com/search?q=etapes+production+produit+de+beaut%C3%A9&rlz=1C1CHBD_frFR868FR868&oq=etapes+production+produit+de+beaut%C3%A9&aqs=chrome..69i57.12025j0j1&sourceid=chrome&ie=UTF-8)
> - [Etape favrication cosmetics](https://www.biotyfullbox.fr/cosmetique-bio/actualite-bio/comment-fabriquer-produit-cosmetique/)
> - [1. Cycle de vie produit cosmetics](https://www.biensdeconso.com/2018/05/29/les-4-etapes-cles-du-cycle-de-vie-d-un-produit-cosmetique/)
> - [Google : Machine fabrication cosmétique à la chaine](https://www.google.com/search?q=machine+fabrication+cosm%C3%A9tique+%C3%A0+la+chaine&rlz=1C1CHBD_frFR868FR868&sxsrf=ALeKk00fyvmaWN88YKcJG5sdmq6qgh7sXw:1610899417763&source=lnms&tbm=isch&sa=X&ved=2ahUKEwj468H8q6PuAhUO4YUKHbv-CosQ_AUoAnoECAkQBA&biw=1920&bih=937)
> - [Machine de production](https://www.inoxmim.com/fr/blog)
> - [Google : étapes machine de fabrication produit cosmétiques](https://www.google.com/search?rlz=1C1CHBD_frFR868FR868&sxsrf=ALeKk03oIgK4bU2YcAWfxZpAcQfRfohdxA%3A1610899464296&ei=CGAEYOHNEYXolwTO0rfIAQ&q=%C3%A9tapes+machine+de+fabrication+produit+cosm%C3%A9tiques&oq=%C3%A9tapes+machine+de+fabrication+produit+cosm%C3%A9tiques&gs_lcp=CgZwc3ktYWIQAzIFCAAQzQIyBQgAEM0CMgUIABDNAjoECAAQRzoHCCMQsAIQJ1CDX1i-ZWDJZmgAcAJ4AIABT4gBpwSSAQE4mAEAoAEBqgEHZ3dzLXdpesgBCMABAQ&sclient=psy-ab&ved=0ahUKEwjh8dmSrKPuAhUF9IUKHU7pDRkQ4dUDCA0&uact=5)
> - [1. Cycle de vie produit cosmetics](https://www.lascom.fr/fiches-pratiques/les-4-etapes-cles-du-cycle-de-vie-dun-produit-cosmetique/)
> - [Deroulement fabricaiton produits](https://cosmeticobs.com/fr/articles/ils-font-les-cosmetiques-8/les-coulisses-de-la-fabrication-dun-cosmetique-794)
> - [Machine industriel frabircation produict cosme](https://www.hellopro.fr/machines-pour-industries-cosmetiques-2008968-fr-1-feuille.html)
> - [Google: fabrication lotion](https://www.google.com/search?q=%C3%A9tapes+fabrications+d%27une+lotion&rlz=1C1CHBD_frFR868FR868&oq=%C3%A9tapes+fabrications+d%27une+lotion&aqs=chrome..69i57.6593j0j1&sourceid=chrome&ie=UTF-8)
> - [Fabrication creme / lotion](https://www.silverson.fr/fr/mediatheque/rapports-dapplication/fabrication-de-cremes-et-lotions-cosmetiques)
