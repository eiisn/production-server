require('./config/config');
require('./config/database');
require('./config/passport');

const express = require('express');
const cors = require('cors');

const app = express();
const passport = require('passport')
const { apiRouter } = require('./src/routes');

const http = require('http').Server(app);
require('./appio')(http)

app.use(express.json())
app.use(cors())
app.use('/favicon.ico', express.static('./favicon.ico'))
app.use(passport.initialize())

app.get('/', (req, res) => {
    res.json({
        "message": "Welcome in production"
    })
})

app.use('/api', apiRouter)

app.use((err, req, res, next) => {
    if (err.name === 'ValidationError') {
        let valErrors = [];
        Object.keys(err.errors).forEach(key => valErrors.push(err.errors[key].message));
        res.status(422).send(valErrors)
    } else {
        console.log(err)
    }
})


http.listen(process.env.PORT, 'localhost', () => {
    console.log(`[Express] Server listening on port ${process.env.PORT}`)
})
