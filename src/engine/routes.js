const { Router } = require('express');
const engineRouter = Router();
const lineRouter = Router();
const ctrlEngine = require('./controller');


engineRouter.get('/e/:_id', ctrlEngine.getEngine)
engineRouter.get('/list', ctrlEngine.getEngines);
engineRouter.get('/running', ctrlEngine.runningEngines);
engineRouter.post('/update', ctrlEngine.updateEngine);
engineRouter.post('/create', ctrlEngine.createEngine);

lineRouter.get('/l/:_id', ctrlEngine.getLine);
lineRouter.get('/list', ctrlEngine.getLines);
lineRouter.get('/list/engines', ctrlEngine.getEnginesByLines);
lineRouter.post('/update', ctrlEngine.updateLine);
lineRouter.post('/create', ctrlEngine.addLine);


module.exports = {
    engineRouter,
    lineRouter
}