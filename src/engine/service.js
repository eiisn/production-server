const ioClient = require('socket.io-client');
const mongoose = require('mongoose');
const Order = mongoose.model('Order');
const Engine = mongoose.model('Engine');
const Line = mongoose.model('Line');

class EngineService {

    constructor() {
        console.log("[EngineService] Contructor");
        this.engineSockets = [];
        this.server = null;
    }

    setServer = (server) => {
        console.log('[EngineService] Set socket io nspServer ...');
        this.server = server;
    }

    openConnexion = async (engine) => {
        console.log(`[EngineService] Open connexion Engine [${engine.name}]: ${engine.ip}:${engine.port}`);
        console.log("[EngineService] Try connection ...");

        const engineConnexion = ioClient(`http://${engine.ip}:${engine.port}/engine`);

        this.server.on('connection', (socket) => {
            console.log(`[${engine.ip}:${engine.port}][${engine.name}][Clien React Socket] connect [${socket.id}]`);
            socket.on('get-status', () => {
                if (engineConnexion.connected) {
                    engineConnexion.emit('get-status');
                } else {
                    this.server.emit('status', {
                        engine: engine._id.toString(),
                        status: false
                    })
                }
            });
        });

        engineConnexion.on('connect', () => {
            console.log(`[${engine.ip}:${engine.port}][${engine.name}] connected`);
            this.addEngine(engine, engineConnexion);
            this.server.emit('status', {
                engine: engine._id.toString(),
                status: true
            });
        });
        
        engineConnexion.on('disconnect', () => {
            console.log(`[${engine.ip}:${engine.port}][${engine.name}] disconnected`);
            this.removeEngine(engine.name);
            this.server.emit('status', {
                engine: engine._id.toString(),
                status: false
            });
        });

        engineConnexion.on('next', (data) => {
            this.server.emit('step', {
                engine: engine._id.toString(),
                step: data
            });
        });

        engineConnexion.on('status', (data) => {
            this.server.emit('status', {
                engine: engine._id.toString(),
                status: data
            });
        });

        engineConnexion.on('health', (health) => {
            console.log(`[${engine.ip}:${engine.port}][${engine.name}] Healed`);
            this.updateHealth(engine.name, health);
            this.server.emit('capacity', {
                engine: engine._id.toString(),
                capacity: health.active + health.waiting
            });
        });

        engineConnexion.on('quantity', (data) => {
            Order.findById(data.order, (err, order) => {
                if (err) console.log(err);
                else {
                    order.produced += data.quantity;
                    order.save((err, orderSaved) => {
                        if (err) console.log(err);
                    });
                }
            });

            Engine.findById(engine._id, (err, engine) => {
                if (err) console.log(err);
                else {
                    engine.quantityProduced += data.quantity;
                    engine.save((err, engineSaved) => {
                        if (err) console.log(err);
                        this.server.emit('quantity', {
                            engine: engineSaved._id.toString(),
                            quantity: engineSaved.quantityProduced
                        });
                    });
                }
            });
        });

        engineConnexion.on('setMaxCapacity', (capacity) => {
            Engine.findById(engine._id, (err, engine) => {
                if (err) console.log(err);
                else {
                    if (engine.maxCapacity !== capacity) {
                        engine.maxCapacity = capacity;
                        engine.save((err, engineSaved) => {
                            if (err) console.log(err);
                        });
                    }
                }
            });
        });

        engineConnexion.on('order-status', (status) => {
            Order.findById(status.order, async (err, order) => {
                if (err) console.log(`[${engine.ip}:${engine.port}][${engine.name}][Order-Status ${status.order}] Error : ${err}`);
                if (status.status === 'produced') {
                    console.log(`[${engine.ip}:${engine.port}][${engine.name}][Order ${order._id}] produced ...`)
                    let engines = await Engine.find({ line: engine.line }).exec();
                    let enginesSorted = engines.sort((a, b) => a.position - b.position); 
                    let index = enginesSorted.findIndex(e => e._id.toString() === engine._id.toString());
                    if (index >= engines.length - 1) {
                        let line = this.getEngine(enginesSorted[index].name).engine.line;
                        console.log(`[${engine.ip}:${engine.port}][${engine.name}][Order ${order._id}] Produced go to quality or stock`);
                        order.status = line.usage === 'stock' ? 'stock' : status.status;
                        order.engineAssigned = null;
                        order.save((err, orderSaved) => {
                            if (err) console.log(err);
                            else console.log(`[${engine.ip}:${engine.port}][${engine.name}][Order ${orderSaved._id}] Status: ${orderSaved.status}`);
                            engineConnexion.emit('check-health');
                        })
                    } else {
                        order.engineAssigned = enginesSorted[index + 1];
                        order.produced = 0;
                        order.save((err, orderSaved) => {
                            if (err) console.log(err)
                            else {
                                let nextEngine = this.getEngine(orderSaved.engineAssigned.name);
                                console.log(`[${engine.ip}:${engine.port}][${engine.name}][Order ${order._id}] Produced go to next Engine : ${nextEngine.engine.name}`)
                                nextEngine.socket.emit('set-order', order);
                                console.log(`[EngineService][Order ${order._id}] job sent to: ${nextEngine.engine.name}`);
                            }
                            engineConnexion.emit('check-health');
                        })
                    }
                } else {
                    order.status = status.status;
                    order.save((err, orderSaved) => {
                        if (err) console.log(err);
                        else console.log(`[${engine.ip}:${engine.port}][${engine.name}][Order ${orderSaved._id}] Status: ${orderSaved.status}`);
                        engineConnexion.emit('check-health');
                    })
                }
            });
        });
        
        engineConnexion.on('order-error', (err) => {
            console.log(`[${engine.ip}:${engine.port}][${engine.name}] Order error: `, err);
        });
    }

    addEngine = async (engine, socket) => {
        this.engineSockets.push({
            engine: {
                ...engine._doc,
                line: await Line.findById(engine.line).exec()
            },
            socket: socket,
            health: {
                waiting: -1,
                active: -1,
                succeeded: -1,
                failed: -1,
                newestJob: -1
            }
        });        
    }

    updateEngine = (engine, engineModified) => {
        this.removeEngine(engine.name);
        this.openConnexion(engineModified);
        return this.getEngine(engineModified.name);
    }

    getEngine = (name) => {
        const index = this.engineSockets.findIndex(detail => detail.engine.name === name);
        return this.engineSockets[index];
    }

    updateHealth = (name, health) => {
        const index = this.engineSockets.findIndex(detail => detail.engine.name === name);
        if (index > -1) {
            this.engineSockets[index].health = health;
            return this.engineSockets[index];
        }
    }

    removeEngine = (name) => {
        const index = this.engineSockets.findIndex(detail => detail.engine.name === name);
        if (index > -1) {
            this.engineSockets.splice(index, 1);
            return this.engineSockets;
        }
    }
}


module.exports = new EngineService();
