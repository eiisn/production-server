const mongoose = require('mongoose');
const Line = mongoose.model('Line')
const Engine = mongoose.model('Engine')
const EngineService = require('../engine/service');


module.exports.getEngine = (req, res, next) => {
    let { _id } = req.params;
    Engine.findById(_id, (err, engine) => {
        if (err) return res.status(400).json(err);
        else return res.status(200).json(engine);
    })
}


module.exports.getEngines = (req, res, next) => {
    Engine.find({}, (err, engines) => {
        if (err) return res.status(400).json(err);
        else {
            let engineMap = []
            engines.forEach((engine) => {
                engineMap.push(engine)
            });
            res.status(200).json(engineMap)
        }
    })
}


module.exports.createEngine = async (req, res, next) => {

    let { ip, port, name, description, position, line } = req.body;

    console.log(req.body);

    let lineObject = await Line.findById(line).exec();
    
    let engine = new Engine({
        ip: ip,
        port: port,
        name: name,
        description: description,
        position: position,
        line: lineObject
    });

    engine.save((err, newEngine) => {
        if (!err) {
            EngineService.openConnexion(newEngine);
            return res.status(200).json(newEngine);
        } else return res.status(400).json(err);
    })
}


module.exports.updateEngine = (req, res, next) => {

    let { _id, newIp, newPort, newName, newDescription, newPosition, newLine } = req.body;

    console.log(req.body)

    Engine.findById(_id, async (err, engine) => {
        if (err) return res.status(400).json(err)
        else {
            if (newIp) engine.ip = newIp;
            if (newPort) engine.port = newPort;
            if (newName) engine.name = newName;
            if (newDescription) engine.description = newDescription;
            if (newPosition) engine.position = newPosition;
            if (newLine) { 
                let line = await Line.findById(newLine).exec();
                engine.line = line;
            }
            engine.save((err, engineModified) => {
                if (err) return res.status(400).json(err);
                else {
                    EngineService.updateEngine(engine, engineModified)
                    return res.status(200).json(engineModified)
                }
            })
        }
    })

}


module.exports.runningEngines = (req, res, next) => {
    res.header('Content-Type', 'application/json');
    return res.send(JSON.stringify({
        engines: EngineService.engineSockets.map(e => {
            return {
                engine: e.engine,
                socket: e.socket.id,
                health: e.health
            }
        })
    }, null, 4))
}


module.exports.getLine = (req, res, next) => {
    let { _id } = req.params;
    Line.findById(_id, (err, line) => {
        if (err) return res.status(400).json(err);
        else return res.status(200).json(line);
    })
}


module.exports.getLines = (req, res, next) => {
    Line.find({}, (err, lines) => {
        let lineMap = []
        lines.forEach((line) => {
            lineMap.push(line)
        });
        return res.status(200).json(lineMap)
    })
}


module.exports.getEnginesByLines = (req, res, next) => {
    Line.find({}, async (err, lines) => {
        let lineMap = []
        if (err) return res.status(400).json(err)
        for (let i = 0; i < lines.length; i++) {
            let engines = await Engine.find({ line: lines[i] });
            engines ? engines : engines = [];
            lineMap.push({
                ...lines[i]._doc,
                engines: engines
            })
        }
        return res.status(200).json(lineMap)
    })
}


module.exports.addLine = (req, res, next) => {
    
    let { name, description, usage } = req.body;

    let line = new Line({
        name: name,
        description: description,
        usage: usage
    })

    line.save((err, newLine) => {
        if (err) return res.status(400).json(err);
        else return res.status(200).json(newLine);
    })
}


module.exports.updateLine = (req, res, next) => {

    let { _id, newName, newDescription, newUsage } = req.body;

    Line.findById(_id, (err, line) => {
        if (err) return res.status(400).json(err);
        else {
            if (newName) {
                line.name = newName,
                line.description = newDescription,
                line.usage = newUsage
            }
            line.save((err, updateLine) => {
                if (err) return res.status(400).json(err);
                else return res.status(200).json(updateLine);
            })
        }
    })
}