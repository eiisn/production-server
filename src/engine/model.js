const mongoose = require('mongoose');


const lineSchema = new mongoose.Schema({
    name: { type: String },
    description: { type: String },
    usage: {
        type: String,
        enum: ['production', 'stock']
    }
});


const engineSchema = new mongoose.Schema({
    ip: { type: String, required: true },
    port: { type: String, required: true },
    name: { type: String, required: true },
    description: { type: String },
    line: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Line"
    },
    position: { type:Number, default: 0 },
    lastStepBeforeShutdown: { type: Number, default: 0 },
    capacity: { type: Number, default: 0 },
    maxCapacity: { type: Number, default: 1 },
    quantityProduced: { type: Number, default: 0 },
    startTime: { type: Date },
    active: { type: Boolean, default: false },
});


mongoose.model('Line', lineSchema);
mongoose.model('Engine', engineSchema);
