const mongoose = require('mongoose')
const jwt = require('jsonwebtoken');

const User = mongoose.model('User');

module.exports.verifyJwtToken = (req, res, next) => {
    let token;
    if ('authorization' in req.headers) {
        token = req.headers['authorization'];
    }
    if (!token) {
        return res.status(403).send({ auth: false, message: 'No token provided.' });
    } else {
        jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
            if (err) {
                return res.status(500).send({ auth: false, message: 'Token authentication failed.' });
            } else {
                User.findById(decoded._id, ['username', 'role'], (err, user) => {
                    if (err) {
                        return res.status(404).json(err)
                    } else {
                        req.user = user;
                        next();
                    }
                });
            }
        })
    }
};
