const { Router } = require('express');

const { verifyJwtToken } = require('./middleware/jwtHelper');
const { orderRouter } = require('./order/routes');
const { engineRouter, lineRouter } = require('./engine/routes');
const { authenticationRouter, userRouter } = require('./user/routes');
const { productRouter, rawMatterRouter } = require('./product/routes');

const apiRouter = Router();

apiRouter.use('/authentication', authenticationRouter);
apiRouter.use('/user', verifyJwtToken, userRouter);
apiRouter.use('/product', productRouter);
apiRouter.use('/raw-matter', rawMatterRouter);
apiRouter.use('/line', lineRouter);
apiRouter.use('/engine', engineRouter);
apiRouter.use('/order', orderRouter);


module.exports = {
    apiRouter
};
