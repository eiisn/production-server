const mongoose = require('mongoose');
const Product = mongoose.model('Product');
const RawMatter = mongoose.model('Raw');


module.exports.addProduct = (req, res, next) => {

    let { code, name, recipe } = req.body;
    let product = new Product({
        code: code,
        name: name,
        recipe: recipe
    })

    product.save((err, newProduct) => {
        if (err) {
            return res.status(400).json(err)
        } else {
            return res.json(newProduct)
        }
    })
}



module.exports.getProducts = (req, res, next) => {
    Product.find({}, async (err, products) => {
        for (let i = 0; i < products.length; i++) {
            for (let j = 0; j < products[i].recipe.length; j++) {
                products[i].recipe[j].rawMatter = await RawMatter.findById(products[i].recipe[j].rawMatter);
            }
        }
        res.status(200).json(products)
    })
}


module.exports.updateProduct = (req, res, next) => {

    let { code, newName, newCode, newRecipe } = req.body;

    Product.findOne({ code: code }, (err, product) => {
        if (newName) {
            product.name = newName
        }
        if (newCode) {
            product.code = newCode
        }
        if (newRecipe) {
            product.recipe = newRecipe
        }
        product.save((err, productModified) => {
            if (err) {
                return res.status(400).json(err)
            } else {
                return res.status(200).json(productModified)
            }
        })
    })
}


module.exports.addRawMatter =  (req, res, next) => {

    let { code, name, quantity } = req.body;
    let raw = new RawMatter({
        code: code,
        name: name,
        stockQuantity: quantity
    })

    raw.save((err, newRaw) => {
        if (err) {
            return res.status(400).json(err)
        } else {
            return res.status(200).json(newRaw)
        }
    })
}


module.exports.getRawMatters = (req, res, next) => {
    RawMatter.find({}, (err, rawMatters) => {
        let rawMattersMap = []
        rawMatters.forEach((rawMatter) => {
            rawMattersMap.push(rawMatter)
        });
        res.status(200).json(rawMattersMap)
    })
}


module.exports.updateRawMatter = (req, res, next) => {

    let { code, newName, newCode, newQuantity } = req.body;

    RawMatter.findOne({ code: code }, (err, rawMatter) => {
        if (err) {
            return res.status(400).json(err)
        } else {
            if (newName) product.name = newName
            if (newCode) product.code = newCode
            if (newQuantity) product.stockQuantity = newQuantity

            rawMatter.save((err, rawMatterModified) => {
                if (err) {
                    return res.status(400).json(err)
                } else {
                    return res.status(200).json(rawMatterModified)
                }
            })
        }
    })
}
