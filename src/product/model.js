const mongoose = require('mongoose');


const productSchema = new mongoose.Schema({
    code: { type: String },
    name: { type: String },
    recipe: [{
        quantity: { type: Number },
        rawMatter : {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Raw'
        }
    }]
})


const rawSchema = new mongoose.Schema({
    code: { type: String},
    name: { type: String },
    stockQuantity: { type: Number }
})


mongoose.model('Raw', rawSchema);
mongoose.model('Product', productSchema);
