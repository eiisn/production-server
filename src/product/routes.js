const { Router } = require('express');
const productRouter = Router();
const rawMatterRouter = Router();
const ctrlProduct = require('./controller');


productRouter.get('/list', ctrlProduct.getProducts);
productRouter.post('/create', ctrlProduct.addProduct);
productRouter.post('/update', ctrlProduct.updateProduct);

rawMatterRouter.get('/list', ctrlProduct.getRawMatters)
rawMatterRouter.post('/create', ctrlProduct.addRawMatter);
rawMatterRouter.post('/update', ctrlProduct.updateRawMatter);


module.exports = {
    productRouter,
    rawMatterRouter
}