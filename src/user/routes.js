const { Router } = require('express');
const authenticationRouter = Router();
const userRouter = Router();
const ctrlUser = require('./controller');

authenticationRouter.post('/login', ctrlUser.authenticate);

userRouter.get('/list', ctrlUser.getUsers)
userRouter.get('/profile', ctrlUser.getCurrentUser)
userRouter.get('/profile/:username', ctrlUser.getUser)
userRouter.post('/register', ctrlUser.register);
userRouter.post('/update', ctrlUser.updateUser)

module.exports = {
    authenticationRouter,
    userRouter
};
