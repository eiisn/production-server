const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');


const userSchema = new mongoose.Schema({
    username: { type: String, unique: true, required: true },
    password: { type: String, required: true, minlength: [8, 'Need at least 8 characters'] },
    passwordModified: { type: Boolean, default: true },
    saltSecret: { type: String, default: '' },
    changePasswordOnConnexion: { type: Boolean, default: true },
    role: {
        type: String,
        default: 'basic',
        enum: ['basic', 'supervisor', 'admin']
    }
})


userSchema.pre('save', function (next) {
    if (this.passwordModified) {
        bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(this.password, salt, (err, hash) => {
                this.password = hash;
                this.saltSecret = salt;
                this.passwordModified = false;
                next();
            });
        });
    } else {
        next();
    }
})


userSchema.methods.verifyPassword = function (password) {
    return bcrypt.compareSync(password, this.password);
}


userSchema.methods.generateJwt = function() {
    return jwt.sign(
        {_id: this._id, username: this.username, role: this.role, passwordChange: this.changePasswordOnConnexion},
        process.env.JWT_SECRET,
        {expiresIn: process.env.JWT_EXP}
    );
}


mongoose.model('User', userSchema)
