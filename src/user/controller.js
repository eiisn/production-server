const mongoose = require('mongoose');
const passport = require('passport');
const generator = require('generate-password');

const { role } = require('./roles')
const User = mongoose.model('User');


module.exports.authenticate = (req, res, next) => {
    passport.authenticate('local', (err, user, info) => {
        if (err) {
            console.log("400 : " + JSON.stringify(err));
            return res.status(400).json(err)
        } else if (user) {
            return res.status(200).json({
                "token": user.generateJwt()
            });
        } else {
            console.log("404: " + JSON.stringify(info));
            return res.status(404).json(info);
        }
    })(req, res);
}


module.exports.register = (req, res, next) => {

    const permission = role.can(req.user.role).createAny('user');

    if (permission.granted) {

        let { username, password, userRole } = req.body;

        if (password === null || password === undefined) {
            password = generator.generate({
                length: 10,
                numbers: true
            })
        }
        
        let user = new User({
            username: username,
            password: password,
            role: userRole
        });
        
        user.save((err, newUser) => {
            if (!err) {
                return res.status(200).json({
                    "username": newUser.username,
                    "password": password,
                    "role": newUser.role
                });
            } else {
                return res.status(400).json(err)
            }
        });
    }
}


module.exports.needPasswordChange = (req, res) => {
    return res.status(200).json({
        change: req.user.changePasswordOnConnexion
    })
}


module.exports.getUser = (req, res) => {
    
    const permission = (req.user.username === req.params.username)
        ? role.can(req.user.role).readOwn('user')
        : role.can(req.user.role).readAny('user')
    
    if (permission.granted) {
        User.findOne({ username: req.params.username }, ['username', 'role'], (err, user) => {
            if (!err) {
                return res.json(user)
            } else {
                return res.status(400).json(err)
            }
        })
    } else {
        return res.status(403).json({
            message: "You don't have permission to see this"
        })
    }
}


module.exports.getUsers = (req, res, next) => {

    const permission = role.can(req.user.role).readAny('user');

    if (permission.granted) {
        User.find({ username: { $ne: req.user.username }}, ['username', 'role'], function (err, users) {
            if (!err) {
                return res.json(users)
            } else {
                return res.status(400).json(err)
            }
        })
    } else {
        return res.status(403).json({
            message: "You don't have permission to see this"
        })
    }
}


module.exports.getCurrentUser = (req, res) => {
    return res.json(req.user)
}


module.exports.updateUser = (req, res) => {

    const permission = (req.user.username === req.body.username)
        ? role.can(req.user.role).updateOwn('user')
        : role.can(req.user.role).updateAny('user')
    
    if (permission.granted) {
        User.findOne({ username: req.body.username }, ['username', 'password', 'role'], (err, user) => {
            if (err)
                return res.status(400).json(err)
            else {
                if (Boolean(req.body.newUsername))
                    user.username = req.body.newUsername
                if (Boolean(req.body.newRole))
                    user.role = req.body.newRole
                if (Boolean(req.body.newPassword)) {
                    user.password = req.body.newPassword
                    user.passwordModified = true
                    if (req.user.username === req.body.username) {
                        user.changePasswordOnConnexion = false
                    } else {
                        user.changePasswordOnConnexion = true
                    }
                }
                user.save((err, updateUser) => {
                    if (err) {
                        return res.status(400).json(err)
                    } else {
                        return res.status(200).json({
                            username: updateUser.username,
                            role: updateUser.role,
                            passwordChange: user.changePasswordOnConnexion
                        })
                    }
                })
            }
        })
    } else {
        return res.status(403).json({
            message: "You don't have permission to update this"
        })
    }
}


module.exports.deleteUser = (req, res) => {
    const permission = (req.user.username !== req.params.username)
        && role.can(req.user.role).deleteAny('user')
    if (permission.granted) {
        User.findOneAndDelete({ username: req.params.username }, (err, user) => {
            if (err)
                return res.status(400).json(err)
            else {
                return res.status(200).json(user)
            }
        })
    } return res.status(403).json({
        message: "You don't have permission to delete this"
    })
}
