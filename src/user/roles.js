const AccessControl = require('accesscontrol');


const ac = new AccessControl();
ac.grant('basic')
    .readOwn('user')
    .updateOwn('user')
    .readAny('engine')
    .createAny('engine')
    .updateAny('engine')
    .readAny('order')
  .grant('supervisor')
    .extend('basic')
    .readAny('user')
    .createAny('user')
    .deleteAny('engine')
    .createAny('order')
    .updateAny('order')
    .deleteAny('order')
  .grant('admin')
    .extend('supervisor')
    .deleteAny('user')
    .updateAny('user')

module.exports = {
    role: ac
}
