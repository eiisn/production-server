const mongoose = require('mongoose')


const orderSchema = mongoose.Schema({
    product: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Product'
    },
    quantity: { type: Number },
    produced: { type: Number, default: 0 },
    status: {
        type: String,
        default: 'queued',
        enum: ['queued', 'in progress', 'produced', 'in validation', 'validate', 'stocked']
    },
    date: { 
        type: Date, 
        default: Date.now 
    },
    engineAssigned: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Engine',
    }
})


mongoose.model('Order', orderSchema)
