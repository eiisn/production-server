const Queue = require('bee-queue');
const mongoose = require('mongoose');
const EngineService = require('../engine/service')
const Engine = mongoose.model('Engine');
const Order = mongoose.model('Order');

class OrderService {

    constructor() {
        console.log("[OrderService] Contructor")
        this.orderQueue = new Queue('order');
        this.orderQueue.process(this.sendOrder);

        setTimeout(() => {
            console.log('Check queued or in progress orders')
            Order.find({ status: { $in: ['queued', 'in progress'] }}, (err, orders) => {
                if (err) console.log(err)
                else orders.forEach(order => {
                    this.makeOrder(order, 'production');
                })
            })
        }, 20000)
    }

    makeOrder = (order, usage) => {
        const job = this.orderQueue.createJob({ order: order, usage: usage });
        console.log("[OrderService] Create bee queue job: ", order._id);
        job.on('failed', (err) => {
            console.log("[OrderService] Order Failed: ", order._id);
            console.log(err);
        });
        job.on('succeeded', async (result) => {
            console.log("[OrderService] Order ", order._id, " job sent to: ", result.engine.name);
            order.engineAssigned = await Engine.findById(result.engine._id).exec();
            order.save();
        });
        job.save();
    }

    sendOrder = (job, done) => {
        console.log("[OrderService][sendOrder][usage] ", job.data.usage);
        let engine = EngineService.engineSockets.filter(e => e.engine.line.usage === job.data.usage).reduce(this.getTheEngine);
        console.log(`[OrderService][sendOrder] Engine found for ${job.data.order._id}: ${engine.engine.name}`);
        if (!engine || engine.health.waiting < 0) {
            console.log('[OrderService][sendOrder] Waiting for available engine ...');
            setTimeout(() => {
                this.sendOrder(job, done);
            }, 15000);
        } else {
            console.log('[OrderService][sendOrder] Set order, ', job.data.order._id);
            engine.socket.emit('set-order', job.data.order);
            return done(null, {
                order: job.data.order,
                engine: engine.engine
            });
        }
    }

    getTheEngine = (prev, curr) => {
        return prev.engine.position < curr.engine.position ? prev : curr;
    }
}


module.exports = new OrderService();
