const mongoose = require('mongoose');
const axios = require('axios');
const Order = mongoose.model('Order');
const Product = mongoose.model('Product');
const Engine = mongoose.model('Engine');
const OrderService = require('./service');


module.exports.createOrder = (req, res, next) => {

    let { productCode, quantity } = req.body;

    Product.findOne({ code: productCode }, (err, product) => {
        let order = new Order({
            product: product,
            quantity: quantity
        });

        order.save((err, newOrder) => {
            if (err) {
                return res.status(400).json(err);
            } else {
                OrderService.makeOrder(newOrder, 'production');
                return res.status(200).json(newOrder);
            }
        })
    })
}


module.exports.getOrders = (req, res, next) => {
    Order.find({}, async (err, orders) => {
        for (let i = 0; i < orders.length; i++) {
            orders[i].product = await Product.findById(orders[i].product);
            orders[i].engineAssigned = await Engine.findById(orders[i].engineAssigned);
        }
        res.status(200).json(JSON.stringify(orders))
    })
}


module.exports.sendToQuality = (req, res, next) => {

    let { order } = req.body;

    Order.findById(order, (err, order) => {
        if (err) return res.status(400).json(err)
        else {
            let esbUrl = process.env.ESB_URL;

            axios.post(`${esbUrl}/quality`, {
                orderId: order._id,
                product: order.product.code,
                quantity: order.produced
            }).then((res) => {
                order.status = 'in validation';
                order.save((err, orderSaved) => {
                    if (err) return res.status(400).json(err);
                    else return res.status(200).json(orderSaved);
                })
            }, (err) => {
                console.log(err);
                order.status = 'in validation';
                order.save((err, orderSaved) => {
                    if (err) return res.status(400).json(err);
                    else return res.status(200).json(orderSaved);
                })
                // return res.status(400).json(err);
            })
        }
    })
}


module.exports.stopOrder = (req, res, next) => {
    return res.json({ message: "OK" })
}
