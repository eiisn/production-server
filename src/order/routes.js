const { Router } = require('express');
const orderRouter = Router();
const ctrlOrder = require('./controller');


orderRouter.get('/list', ctrlOrder.getOrders);
orderRouter.post('/create', ctrlOrder.createOrder);
orderRouter.post('/quality', ctrlOrder.sendToQuality);


module.exports = {
    orderRouter
}